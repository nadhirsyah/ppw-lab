from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Nadhirsyah' # TODO Implement this
lahir = "1999"
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 7, 18) #TODO Implement this, format (Year, Month, Date)
npm = 1706039383 # TODO Implement this
kuliah = 'Universitas Indonesia'
hobi_1 = 'main game'

mhs_name_2 = "Mohammad Adli Daffa Wirapanatayudha"
birth_date_2 = date(1999, 5, 2 )
npm_2 = 1706984663
hobi_2 = 'teka-teki'
desc_2 = "Assalamualaikum. Nama gua Mohammad Adli, biasa dipanggil adli. Milih fasilkom ui karena prospek " \
         "pekerjaannya pas lulus nanti lumayan bagus, padahal sebelumnya pengen di fmipa ui. Gua suka misteri atau " \
         "teka-teki, pas masuk fasilkom tersalurkan lah kesukaan gua. Banyak misteri di fasilkom termasuk juga banyak " \
         "teka teki di mata kuliah yang ada di dalamnya. Gua harap gua bisa lulus dan dapat bekerja di perusahaan it " \
         "yang sesuai dengan bakat gua yang insyaAllah didapatkan di fasilkom. "

mhs_name_3 = "Adam Maulana"
birth_date_3 = date(1999, 3, 26)
npm_3 = 1706043651
hobi_3 = "membaca"
desc_3 = "Kenalin nama gue Adam, gua masuk sistem informasi fasilkom karena gua diterima di sistem informasi fasilkom. Cita cita gua jadi iron man. " \
         "Keseharian gua itu ya mencoba lebih baik dari hari sebelumnya, walau kadang ga lebih baik, seringnya lebih ga baik, karena kebaikan datangnya dari Allah."

# Create your views here.
def index(request):
    response = {'name_1': mhs_name, 'age_1': calculate_age(birth_date.year), 'npm_1': npm, 'kuliah':kuliah, "tanggal_1":birth_date, "hobi_1": hobi_1,
                'name_2': mhs_name_2, 'age_2': calculate_age(birth_date_2.year), 'npm_2': npm_2,
                "tanggal_2": birth_date_2, "hobi_2": hobi_2, 'name_3': mhs_name_3, 'age_3': calculate_age(birth_date_3.year),
                'npm_3': npm_3, "tanggal_3":birth_date_3, "hobi_3": hobi_3, "desc_2": desc_2, "desc_3":desc_3, "lahir":lahir}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
